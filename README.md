# Walidator PESEL
### Co to Walidator PESEL?

Walidator PESEL to program napisany w języku JAVA który na podstawie podanego numeru PESEL pozwala na sprawdzenie jego poprawności oraz uzyskanie informacji
### Funkcje programu
- Sprawdzanie czy podana dana to faktycznie numer PESEL
- Wyświetlanie danych
### Informacje wyświetlane 
- Dzień urodzin
- Miesiąc urodzin (słownie)
- Rok urodzin
- płeć
- Data urodzenia w formacie DD-MM-YYYY

### Użyte technologie oraz narzędzia
- IntelliJ IDEA od JetBrains - IDE
- openjdk-17.0.1 - SKD
- GIT - system kontroli wersji
- Bitbucket od Atlassian - zdalne repozytorium GIT
- Stack Overflow - pomoc w rowiązywaniu problemów
- dillinger.io - generowanie README

### Uruchamianie

Walidator PESEL potrzebuje środowiska [intelliJ IDEA](https://www.jetbrains.com/idea/) do uruchomienia

```sh
git clone git@bitbucket.org:michal43/project_01_34318.git
```
Następnie uruchom środowisko inteliJ IDEA i kliknij "run" w prawym górnym rogu

### Skład zespołu i podział prac
| Pikusa Michał | 34318 | Wszystko |
|Abramowicz Jan|34266|Reszta|
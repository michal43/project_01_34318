package michalpikusa;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class pesel {
    String pesel;
    String day;
    int month;
    String month_string;
    String month_name;
    String year;
    String sex;
    Scanner scn = new Scanner(System.in);

    public void get(){
        System.out.print("Podaj numer PESEL: ");
        pesel=scn.nextLine();
    }

    public boolean check(){
        if(pesel.length()==11){
            if(pesel.matches("[0-9]+")){
                if(check_sum()==Character.getNumericValue(pesel.charAt(10))){
                    return true;
                }
                else{
                    return false;
                }

            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public void birth_date(){
        day=pesel.substring(4,6);
        month=parseInt(pesel.substring(2,4));
        year=pesel.substring(0,2);
        int century=Character.getNumericValue(pesel.charAt(2));

        if(century == 8 || century == 9){
            year="18"+year;
            month=month-80;

        }
        else if(century == 0 || century== 1){
            year="19"+year;
        }
        else if(century == 2 || century== 3){
            year="20"+year;
            month=month-20;
        }
        else if(century == 4 || century== 5){
            year="21"+year;
            month=month-40;
        }
        else if(century == 6 || century== 7){
            year="22"+year;
            month=month-60;
        }

        if(month<10){
            month_string =String.valueOf(month);
            month_string ="0"+ month_string;
        }
    }

    public void sex(){
        char sex_digit=pesel.charAt(9);
        if(sex_digit%2==0){
            sex="Kobiety";
        }
        else{
            sex="Mężczyzy";
        }
    }

    public int check_sum(){
        int[] wages = {1,3,7,9,1,3,7,9,1,3};
        int[] products = new int[10];
        int check_sum=0;

        for(int i=0;i<=9;i++){
            products[i]=Character.getNumericValue(pesel.charAt(i))*wages[i];
            check_sum=check_sum+products[i];
        }
        check_sum=check_sum%10;
        check_sum=10-check_sum;
        return check_sum;


    }

    public void month_name(){
        List<String> name = new LinkedList<>();
        name.add("Styczeń");
        name.add("Luty");
        name.add("Marzec");
        name.add("Kwiecień");
        name.add("Maj");
        name.add("Czerwiec");
        name.add("Lipiec");
        name.add("Sierpień");
        name.add("Wrzesień");
        name.add("Październik");
        name.add("Listopad");
        name.add("Grudzień");

        month_name =name.get(month-1);
    }

    public void print(){
        birth_date();
        sex();
        month_name();
        System.out.println("Dzień urodzin: "+day);
        System.out.println("Miesiąc urodzin: "+ month_name);
        System.out.println("Rok urodzin: "+year);
        System.out.println("Podany PESEL należy do: "+sex);
        System.out.println("Data urodzenia: "+day+"-"+ month_string +"-"+year);
    }
}
